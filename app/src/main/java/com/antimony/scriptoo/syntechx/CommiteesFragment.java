package com.antimony.scriptoo.syntechx;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by antimony on 12/11/17.
 */

public class CommiteesFragment extends Fragment {


    ListView listView;
    ArrayList<CommiteeClass> array;
    BottomNavigationView bottom;


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        listView = getActivity().findViewById(R.id.commitees_list_view);
        bottom = getActivity().findViewById(R.id.main_bottom_navigation);
        DatabaseHelper db = new DatabaseHelper(getContext());
        array = db. getCommitees();
        db.close();
        CommiteesAdapter adapter = new CommiteesAdapter(getContext(), array);
        View header = (View)getLayoutInflater().inflate(R.layout.header_commitees_listview, null);
        listView.addHeaderView(header);
        listView.setAdapter(adapter);

        ((MainActivity)getActivity()).setColors(getResources().getColor(R.color.commitees_blue), getResources().getColor(R.color.commitees_bluedark));


    }

    public static CommiteesFragment getInstance(){

        CommiteesFragment fragment = new CommiteesFragment();
        return fragment;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_commitees, container, false);
    }
}
