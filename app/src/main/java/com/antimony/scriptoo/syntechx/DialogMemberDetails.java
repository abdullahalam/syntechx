package com.antimony.scriptoo.syntechx;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DialogTitle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by antimony on 12/17/17.
 */

public class DialogMemberDetails extends AppCompatActivity {


    Intent recievedIntent;
    TextView name;
    CircleImageView dp;
    DialogTitle title;
    String number, email;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.member_details_card_dialog);



        recievedIntent = getIntent();
        name = findViewById(R.id.member_dialog_name);
        dp = findViewById(R.id.member_dialog_image);

        setTitle(recievedIntent.getStringExtra("EVENTNAME"));
        name.setText(recievedIntent.getStringExtra("MEMBERNAME"));

        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.displayImage(recievedIntent.getStringExtra("IMAGEURL"), dp);


        number = "tel:" + recievedIntent.getStringExtra("MEMBERNUMBER");
        email = "mailto:" + recievedIntent.getStringExtra("MEMBEREMAIL");


    }


    public void callLaga(View v){

        if(recievedIntent.getStringExtra("MEMBERNUMBER")!=null) {
            Intent i = new Intent(Intent.ACTION_DIAL);
            i.setData(Uri.parse(number));
            startActivity(i);
        }else {
            Toast.makeText(this, "Number unavailable", Toast.LENGTH_SHORT).show();
        }
    }

    public void emailKar(View v){
        if(recievedIntent.getStringExtra("MEMBEREMAIL")!=null) {
            Intent i = new Intent(Intent.ACTION_SENDTO);
            i.setData(Uri.parse(email));
            startActivity(i);
        }else{
            Toast.makeText(this, "Email unavailable", Toast.LENGTH_SHORT).show();
        }

    }

}
