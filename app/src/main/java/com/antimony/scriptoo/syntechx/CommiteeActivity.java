package com.antimony.scriptoo.syntechx;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.nostra13.universalimageloader.core.ImageLoader;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class CommiteeActivity extends AppCompatActivity {

    Intent intent;
    TextView title, role;
    LinearLayout linearLayout;
    ArrayList<MemberClass> array;
    String id;
    Toolbar toolbar;
    CommiteeClass com;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commitee);


        intent = getIntent();
        title = findViewById(R.id.commitee_page_title);
        role = findViewById(R.id.commitee_page_role);
        toolbar = findViewById(R.id.commitee_activity_toolbar);

        com = (CommiteeClass)intent.getSerializableExtra("COMMITEEOBJECT");

        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        title.setText(com.name);
        role.setText(com.role);


//        title.setText(intent.getStringExtra("NAME"));
//        role.setText(intent.getStringExtra("ROLE"));
        id= com.id;
        linearLayout = findViewById(R.id.head_list_linearlayout);

        addTeam();

    }



    public void addTeam(){



        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        array = databaseHelper.getMembers(id);


        for (int count = 0; count < array.size(); count++){


            View row = getLayoutInflater().inflate(R.layout.commitee_member_entry_layout, null);
            linearLayout.addView(row);

            //GET INSTANCE
            CircleImageView imageView = row.findViewById(R.id.commitee_member_image);
            TextView textView = row.findViewById(R.id.commitee_member_name);

            //SET IMAGE
            ImageLoader imageLoader = ImageLoader.getInstance();
            final String url = array.get(count).image_url;
            if(url != null) {
                imageLoader.displayImage(array.get(count).image_url, imageView);
            }

            //SET NAME
            textView.setText(array.get(count).name);

            //FINAL DETAIL TRINGS
            final String naam = array.get(count).name;
            final String comname = com.name;
            final String number = array.get(count).contact_num;

            final String email = array.get(count).email_id;

            //ADD LISTENER
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getApplicationContext(), DialogMemberDetails.class);

                    //EXTRAS
                    i.putExtra("EVENTNAME", comname);
                    i.putExtra("MEMBERNAME", naam);
                    i.putExtra("MEMBERNUMBER", number);
                    i.putExtra("MEMBEREMAIL", email);
                    i.putExtra("IMAGEURL", url);

                    startActivity(i);
                }
            });

        }

        databaseHelper.close();

    }


    @Override
    public boolean onSupportNavigateUp() {

        finish();
        return true;
    }
}
