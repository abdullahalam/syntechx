package com.antimony.scriptoo.syntechx;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by antimony on 12/5/17.
 */

public class AboutFragment extends Fragment {

    Toolbar toolbar;
    Button website_button, developer_button;
    final String website_url = "http://syntech-x.rdnational.ac.in/";


    public static AboutFragment getInstance(){
        AboutFragment fragment = new AboutFragment();
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbar = (Toolbar)getActivity().findViewById(R.id.about_page_toolbar);
        toolbar.setTitle(getString(R.string.app_name));

        website_button = getActivity().findViewById(R.id.website_button);
        website_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(website_url));
                startActivity(i);
            }
        });

        developer_button = getActivity().findViewById(R.id.developer_button);
        developer_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://goo.gl/5WvbrQ"));
                startActivity(i);
            }
        });

        ((MainActivity)getActivity()).setColors(
                getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorPrimaryDark)
        );
    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_about, container, false);

    }
}
