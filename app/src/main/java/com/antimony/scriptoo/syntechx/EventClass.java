package com.antimony.scriptoo.syntechx;

import android.database.Cursor;

import com.antimony.scriptoo.syntechx.DatabaseHelper;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by antimony on 12/10/17.
 */

public class EventClass implements Serializable {

    public String name, rules, description, timing, date, location, background;

    public EventClass(Cursor c){

        name = c.getString(c.getColumnIndex(DatabaseHelper.EVENT_NAME));
        background = c.getString(c.getColumnIndex(DatabaseHelper.EVENT_BACKGROUND));
        rules = c.getString(c.getColumnIndex(DatabaseHelper.EVENT_RULES));
        description = c.getString(c.getColumnIndex(DatabaseHelper.EVENT_DESCRIPTION));
        timing = c.getString(c.getColumnIndex(DatabaseHelper.EVENT_TIMINGS));
        date = c.getString(c.getColumnIndex(DatabaseHelper.EVENT_DATE));
        location = c.getString(c.getColumnIndex(DatabaseHelper.EVENT_LOCATION));




    }

}
