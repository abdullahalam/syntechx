package com.antimony.scriptoo.syntechx;

import android.content.Intent;
import android.database.SQLException;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    BottomNavigationView bottom;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottom = findViewById(R.id.main_bottom_navigation);
        listenToNavigation();


        //DB LOADER INIT
        DisplayImageOptions displayImageOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(displayImageOptions)
                .build();

        ImageLoader.getInstance().init(config);

        //DB INIT
        DatabaseHelper dh = new DatabaseHelper(this);

        //CREATE
        try{
            dh.createDatabase();
        }catch (IOException ioe){
            throw new Error("Unable to create");
        }



        setFragment(EventsFragment.getInstance());

    }

    public void listenToNavigation(){
        bottom.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {


                Fragment selected = null;

                switch (item.getItemId()){

                    //ABOUT PAGE
                    case R.id.navigation_about:
                        selected = AboutFragment.getInstance();
                        break;



                    //COMMITEES PAGE
                    case R.id.navigation_commitees:
                        selected = CommiteesFragment.getInstance();
                        break;



                    //GALLERY PAGE
                    case R.id.navigation_gallery:
                        selected = GalleryFragment.getInstance();
                        break;



                    case R.id.navigation_events:
                        selected = EventsFragment.getInstance();//
                        break;



                }

                setFragment(selected);
                return true;

            }
        });
    }


    public void setColors(int primary, int primaryDark){

        bottom.setBackgroundColor(primary);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(primaryDark);
        }

    }

    public void setFragment(Fragment selected){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main_frame_layout, selected);
        transaction.commit();

    }


}
