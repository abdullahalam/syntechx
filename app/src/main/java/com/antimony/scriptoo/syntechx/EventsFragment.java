package com.antimony.scriptoo.syntechx;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by antimony on 12/10/17.
 */

public class EventsFragment extends Fragment {


    Toolbar toolbar;
    ListView listView;
    ArrayList<EventClass> array;
//    NestedScrollView nestedScrollView;

    public static EventsFragment getInstance(){
        EventsFragment fragment = new EventsFragment();
        return fragment;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbar = (Toolbar)getActivity().findViewById(R.id.events_toolbar);
        listView = (ListView)getActivity().findViewById(R.id.events_fragment_listview);

        DatabaseHelper db = new DatabaseHelper(getContext());
        array = db.getEvents();
        db.close();
        EventsAdapter adapter = new EventsAdapter(getContext(), array);
        listView.setAdapter(adapter);

        toolbar.setTitle("Events");
        toolbar.setBackgroundColor(getResources().getColor(R.color.grey));
        ((MainActivity)getActivity()).setColors(
                getResources().getColor(R.color.grey),
                getResources().getColor(R.color.grey)

        );

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_events, container, false);
    }
}
