package com.antimony.scriptoo.syntechx;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ViewFlipper;

import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by antimony on 12/17/17.
 */

public class GalleryFragment extends Fragment {



    public ViewFlipper simpleViewFlipper;
    public ViewFlipper simpleViewFlipper2;
    public ViewFlipper simpleViewFlipper3;
    public Animation in, out;



    public void members(){
        // get The references of ViewFlipper
        simpleViewFlipper = (ViewFlipper) getActivity().findViewById(R.id.view_flipper_member); // get the reference of ViewFlipper
        String[] Members = getResources().getStringArray(R.array.Members);


        // loop for creating ImageView's
        for (int i = 0; i < Members.length; i++) {
            // create the object of ImageView
            ImageLoader imageLoader = ImageLoader.getInstance();
            ImageView imageView = new ImageView(getContext());
            imageLoader.displayImage(Members[i], imageView);
            simpleViewFlipper.addView(imageView);
        }
        // Declare in and out animations and load them using AnimationUtils class

        simpleViewFlipper.setInAnimation(in);
        simpleViewFlipper.setOutAnimation(out);
        simpleViewFlipper.setFlipInterval(3000);
        simpleViewFlipper.setAutoStart(true);
        simpleViewFlipper.startFlipping();


    }

    public void flipper16(){
        // get The references of ViewFlipper
        simpleViewFlipper = (ViewFlipper) getActivity().findViewById(R.id.view_flipper_16); // get the reference of ViewFlipper
        String[] y2016 = getResources().getStringArray(R.array.y2016);


        // loop for creating ImageView's
        for (int i = 0; i < y2016.length; i++) {
            // create the object of ImageView
            ImageLoader imageLoader = ImageLoader.getInstance();
            ImageView imageView = new ImageView(getContext());
            imageLoader.displayImage(y2016[i], imageView);
            simpleViewFlipper.addView(imageView);
        }
        // Declare in and out animations and load them using AnimationUtils class

        simpleViewFlipper.setInAnimation(in);
        simpleViewFlipper.setOutAnimation(out);
        simpleViewFlipper.setFlipInterval(3000);
        simpleViewFlipper.setAutoStart(true);
        simpleViewFlipper.startFlipping();


    }

    public void flipper15(){
        // get The references of ViewFlipper
        simpleViewFlipper2 = (ViewFlipper) getActivity().findViewById(R.id.view_flipper_15); // get the reference of ViewFlipper
        String[] y2015 = getResources().getStringArray(R.array.y2015);


        // loop for creating ImageView's
        for (int i = 0; i < y2015.length; i++) {
            // create the object of ImageView
            ImageLoader imageLoader = ImageLoader.getInstance();
            ImageView imageView = new ImageView(getContext());
            imageLoader.displayImage(y2015[i], imageView);
            simpleViewFlipper2.addView(imageView);
        }
        // Declare in and out animations and load them using AnimationUtils class
        simpleViewFlipper2.setInAnimation(in);
        simpleViewFlipper2.setOutAnimation(out);
        simpleViewFlipper2.setFlipInterval(3500);
        simpleViewFlipper2.setAutoStart(true);
        simpleViewFlipper2.startFlipping();

    }

    public void flipper13(){
        // get The references of ViewFlipper
        simpleViewFlipper2 = (ViewFlipper) getActivity().findViewById(R.id.view_flipper_13); // get the reference of ViewFlipper
        String[] y2013 = getResources().getStringArray(R.array.y2013);


        // loop for creating ImageView's
        for (int i = 0; i < y2013.length; i++) {
            // create the object of ImageView
            ImageLoader imageLoader = ImageLoader.getInstance();
            ImageView imageView = new ImageView(getContext());
            imageLoader.displayImage(y2013[i], imageView);
            simpleViewFlipper2.addView(imageView);
        }
        // Declare in and out animations and load them using AnimationUtils class
        simpleViewFlipper2.setInAnimation(in);
        simpleViewFlipper2.setOutAnimation(out);
        simpleViewFlipper2.setFlipInterval(4000);
        simpleViewFlipper2.setAutoStart(true);
        simpleViewFlipper2.startFlipping();

    }

    public void flipper14(){
        // get The references of ViewFlipper
        simpleViewFlipper2 = (ViewFlipper) getActivity().findViewById(R.id.view_flipper_14); // get the reference of ViewFlipper
        String[] y2014 = getResources().getStringArray(R.array.y2014);


        // loop for creating ImageView's
        for (int i = 0; i < y2014.length; i++) {
            // create the object of ImageView
            ImageLoader imageLoader = ImageLoader.getInstance();
            ImageView imageView = new ImageView(getContext());
            imageLoader.displayImage(y2014[i], imageView);
            simpleViewFlipper2.addView(imageView);
        }
        // Declare in and out animations and load them using AnimationUtils class
        simpleViewFlipper2.setInAnimation(in);
        simpleViewFlipper2.setOutAnimation(out);
        simpleViewFlipper2.setFlipInterval(4500);
        simpleViewFlipper2.setAutoStart(true);
        simpleViewFlipper2.startFlipping();

    }



    public static GalleryFragment getInstance(){
        GalleryFragment fragment = new GalleryFragment();
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((MainActivity)getActivity()).setColors(
                getResources().getColor(R.color.gallery_teal),
                getResources().getColor(R.color.gallery_tealdark)
        );

        in = AnimationUtils.loadAnimation(getContext(), android.R.anim.slide_in_left);
        out = AnimationUtils.loadAnimation(getContext(), android.R.anim.slide_out_right);

        flipper16();
        flipper15();
        flipper13();
        flipper14();
        members();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_gallery, container, false);
    }
}
