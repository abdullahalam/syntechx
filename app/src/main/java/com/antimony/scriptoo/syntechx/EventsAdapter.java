package com.antimony.scriptoo.syntechx;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

public class EventsAdapter extends ArrayAdapter<EventClass> {


    private static class ViewHolder{
        TextView name;
        ImageView image;
        RelativeLayout root;
    }


    public EventsAdapter(Context context, ArrayList<EventClass> events){
        super(context, 0, events);
    }


    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {


        final EventClass event = getItem(position);
        //GET EVENT FROM ARRAYADAPTER

        ViewHolder viewHolder;

        if(convertView == null){

            viewHolder = new ViewHolder();

            convertView = LayoutInflater.from(getContext()).inflate(R.layout.event_card, parent, false);

            viewHolder.name = convertView.findViewById(R.id.event_card_title);
            viewHolder.image = convertView.findViewById(R.id.events_card_background);
            viewHolder.root = convertView.findViewById(R.id.event_card_root);

            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        //INFLATE THE VIEW



        viewHolder.name.setText(event.name);




        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.displayImage(event.background, viewHolder.image);


        viewHolder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), EventActivity.class);

                i.putExtra("EVENTOBJECT", event);

                getContext().startActivity(i);
            }
        });



        return convertView;
    }
}
