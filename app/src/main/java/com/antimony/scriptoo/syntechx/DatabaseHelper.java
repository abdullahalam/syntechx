package com.antimony.scriptoo.syntechx;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

/**
 * Created by antimony on 12/6/17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {



    public static DatabaseHelper object = null;

    private static String DB_PATH = "/data/data/com.antimony.scriptoo.syntechx/databases/";
    private static String DB_NAME = "syntechx";
    private static int DBVERSION = 92;

    //MEMBERS COLUMNS
    public static String MEMBER_NAME = "name";
    public static String MEMBER_CONTACT_NUMBER = "contact_number";
    public static String MEMBER_EMAIL = "contact_mail";
    public static String MEMBER_IMAGE_URL = "image_url";

    //COMMITEES COLUMNS
    public static String COMMITEE_NAME = "name";
    public static String COMMITEE_ROLE = "role";
    public static String COMMITEE_LOGO = "logo";
    public static String COMMITEE_ID = "_id";

    //EVENT COLUMNS
    public static String EVENT_NAME = "name";
    public static String EVENT_DESCRIPTION = "description";
    public static String EVENT_RULES = "rules";
    public static String EVENT_TIMINGS = "timings";
    public static String EVENT_LOCATION = "location";
    public static String EVENT_BACKGROUND = "background";
    public static String EVENT_DATE = "date";

    private SQLiteDatabase myDatabase;
    private final Context myContext;


    public DatabaseHelper(Context context){
        super(context, DB_NAME, null, DBVERSION);
        this.myContext = context;

        try{

            try{
                createDatabase();
            }catch (IOException e){
                Log.d("DB", "DatabaseHelper(createDatabse)");
            }
            openDatabase();
        }catch (SQLException sqle){
            throw sqle;
        }

    }

    public void createDatabase() throws IOException{
        boolean dbExist = checkDatabase();
//        boolean dbExist = false;


        if(dbExist){
            //kuch mat kar; database hai
        }else{
            this.getReadableDatabase();

            try {
                copyDataBase();
            }catch (IOException e){
                throw new Error("Error copying database");
            }
        }

    }

    private boolean checkDatabase(){
        SQLiteDatabase checkDB = null;

        try{
            String myPath = DB_PATH + DB_NAME;
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);

        }catch (SQLiteException e){
            //db nahi hai
        }

        if(checkDB !=null){
//            checkDB.close();
        }

        return checkDB !=null;
    }

    private void copyDataBase() throws IOException{

        InputStream myInput = myContext.getAssets().open(DB_NAME);
        String outFileName = DB_PATH + DB_NAME;
        OutputStream myOutput = new FileOutputStream(outFileName);

        byte[] buffer = new byte[16384];
        int length;
        while((length = myInput.read(buffer))>0){
            myOutput.write(buffer, 0, length);
        }

        myOutput.flush();
        myOutput.close();
        myInput.close();
//        this.close();
    }

    public void openDatabase() throws SQLException{
        String myPath = DB_PATH + DB_NAME;
        myDatabase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);

    }

    @Override
    public synchronized void close() {

        if(myDatabase != null){
            myDatabase.close();
        }
        super.close();
    }

    public ArrayList<EventClass> getEvents(){


        ArrayList<EventClass> array = new ArrayList<>();


        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM events;";

        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();

        do{
            array.add(new EventClass(c));
        }while (c.moveToNext());

        c.close();

        return array;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

       //KUCH TOH
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(newVersion>oldVersion){
            try {
                copyDataBase();
            }catch (IOException e){
                Log.d("DB", "Oops");
            }
        }
    }



    public ArrayList<CommiteeClass> getCommitees(){
        ArrayList<CommiteeClass> array = new ArrayList<>();


        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM commitees;";

        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();

        do{
            array.add(new CommiteeClass(c));
        }while (c.moveToNext());

        c.close();

        return array;
    }

    public ArrayList<MemberClass> getMembers(String id){

        ArrayList<MemberClass> array = new ArrayList<>(1);

        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM members WHERE commitee_id = " + id + " ORDER BY " + MEMBER_NAME +";";

        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();


        do{
            array.add(new MemberClass(c));
        }while(c.moveToNext());

        c.close();

        return array;

    }

}
