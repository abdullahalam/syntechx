package com.antimony.scriptoo.syntechx;

import android.database.Cursor;
import android.provider.ContactsContract;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by antimony on 12/11/17.
 */

public class CommiteeClass implements Serializable{


    String name, role, image, id;



    public CommiteeClass(Cursor c){

        id = Integer.toString(c.getInt(c.getColumnIndex(DatabaseHelper.COMMITEE_ID)));
//        id = "5";
        name = c.getString(c.getColumnIndex(DatabaseHelper.COMMITEE_NAME));
        role = c.getString(c.getColumnIndex(DatabaseHelper.COMMITEE_ROLE));
        image = c.getString(c.getColumnIndex(DatabaseHelper.COMMITEE_LOGO));

    }


}
