package com.antimony.scriptoo.syntechx;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;



public class EventActivity extends AppCompatActivity {


    TextView description, rules, location, time;
    ImageView header;
    Toolbar toolbar;




    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_page);



        toolbar = (Toolbar) findViewById(R.id.event_page_toolbar);
        description = (TextView)findViewById(R.id.description_textview);
        rules= (TextView)findViewById(R.id.rules_textview);
        time= findViewById(R.id.event_timing_view);
        location= (TextView)findViewById(R.id.location_textview);

        header = (ImageView)findViewById(R.id.event_page_header);
        Intent i = getIntent();



        setSupportActionBar(toolbar);
//
//
        EventClass event = (EventClass)i.getSerializableExtra("EVENTOBJECT");


        //SET ALL THE THINGS
        getSupportActionBar().setTitle(event.name);
        description.setText(event.description);
        rules.setText(event.rules);
        time.setText(event.date + ", " + event.timing);
        location.setText(event.location);

        ImageLoader loader = ImageLoader.getInstance();
        loader.displayImage(event.background, header);


        //TRANSPARENT STATUS BAR
       /* if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }*/



    }


    public void register(View v){

        Intent i = new Intent(this, RegistrationActivity.class);
        startActivity(i);

    }




}
