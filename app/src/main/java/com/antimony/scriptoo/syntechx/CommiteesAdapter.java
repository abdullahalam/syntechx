package com.antimony.scriptoo.syntechx;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by antimony on 12/11/17.
 */

public class CommiteesAdapter extends ArrayAdapter<CommiteeClass>{


    private static class ViewHolder{
        TextView name;
        ImageView image;
    }

    public CommiteesAdapter(Context context, ArrayList<CommiteeClass> coms){
        super(context, 0, coms);
    }


    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        final CommiteeClass com = getItem(position);
        ViewHolder viewHolder;

        if (convertView == null){
            viewHolder = new ViewHolder();

            convertView = LayoutInflater.from(getContext()).inflate(R.layout.commitee_item_layout, parent, false);

            viewHolder.name = convertView.findViewById(R.id.commitee_item_text);
            viewHolder.image = convertView.findViewById(R.id.commitee_item_logo);

            convertView.setTag(viewHolder);

        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.name.setText(com.name);
        viewHolder.image.setImageResource(getContext().getResources().getIdentifier(
                com.image.split("\\.png")[0],
                "drawable",
                getContext().getPackageName())
        );

//        Log.d("LOL", com.image.split("\\.png")[0]);

        viewHolder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), CommiteeActivity.class);
//                i.putExtra("NAME", com.name);
//                i.putExtra("ROLE", com.role);
                i.putExtra("ID", position);
                i.putExtra("COMMITEEOBJECT", com);

                getContext().startActivity(i);
            }
        });
        return convertView;

    }
}
